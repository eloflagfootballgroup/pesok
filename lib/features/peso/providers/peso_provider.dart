import 'dart:math';

import 'package:hive/hive.dart';
import 'package:pesok/features/peso/models/peso_model_hive.dart';
import 'package:pesok/features/peso/providers/peso_repo_provider.dart';
import 'package:pesok/features/peso/repo/peso_repo.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'dart:developer' as developer;
import 'package:uuid/uuid.dart';

part 'peso_provider.g.dart';

var _uuid = Uuid();

@riverpod
class Peso extends _$Peso {
  late PesoRepo? repo;

  @override
  List<PesoModelHive>? build() {
    repo = ref.watch(pesoRepoProvider);
    return repo!.getValue();
  }

  void addPeso(String pesoString) {
    double pesoMedio = 0;
    List<PesoModelHive> listaPesosLast4Days = [];
    double sumaPeso5Dias = 0.0;
    if (state!.length >= 5) {
      //listaPesosLast4Days = state.reversed.take(4).toList();
      listaPesosLast4Days = state!.sublist(state!.length - 4);

      for (PesoModelHive peso in listaPesosLast4Days) {
        sumaPeso5Dias += peso.peso!;
      }
      pesoMedio = (sumaPeso5Dias + double.parse(pesoString)) / 5;
    }
    final nuevoPeso = PesoModelHive(
      fechaDeCreacion: DateTime.now(),
      peso: double.parse(pesoString),
      pesoMedio: pesoMedio,
    );
    state = repo!.addValue(nuevoPeso);
  }

  /// Actualizacion del peso mediante riverpod
  void updatePeso(int index, PesoModelHive pesoToUpdate) {
    // Sustituimos el peso por el nuevo
    state = repo!.updateValue(index, pesoToUpdate);

    // Se reordena la lista en funcion de la fecha
    state!.sort((a, b) => a.fechaDeCreacion!.compareTo(b.fechaDeCreacion!));
    double suma = 0.0;
    List<PesoModelHive> listActualizada = [];

    void agregarPeso(PesoModelHive peso) {
      if (listActualizada.length < 5) {
        listActualizada.add(peso.copyWith(pesoMedio: 0.0));
      } else {
        listActualizada.add(peso.copyWith(pesoMedio: suma / 5));
        suma -= listActualizada[listActualizada.length - 5].peso!;
      }
    }

    for (PesoModelHive peso in state!) {
      suma += peso.peso!;
      if (peso.id == pesoToUpdate.id) {
        agregarPeso(pesoToUpdate);
      } else {
        agregarPeso(peso);
      }
    }

    state = listActualizada;
  }

  void deletePeso(id) {
    state = repo!.removeValue(id);

    double suma = 0.0;
    List<PesoModelHive> listActualizada = [];

    void agregarPeso(PesoModelHive peso) {
      if (listActualizada.length < 5) {
        listActualizada.add(peso.copyWith(pesoMedio: 0.0));
        // developer.log(
        //     '${listActualizada.indexOf(peso)} - Peso: ${peso.peso} \n    Suma: $suma');
      } else {
        developer.log("Suma antes: $suma + peso: ${peso.peso}");
        developer.log(
            "Peso - 5: ${listActualizada[listActualizada.length - 5].peso}");
        listActualizada.add(peso.copyWith(pesoMedio: suma / 5));
        suma -= listActualizada[listActualizada.length - 5].peso!;
        developer.log("Suma despues: $suma");
        // developer.log(
        //     '${listActualizada.indexOf(peso)} - Peso: ${peso.peso} \n    Suma: $suma');
      }
    }

    for (PesoModelHive peso in state!) {
      suma += peso.peso!;

      agregarPeso(peso);
    }

    state = listActualizada;
  }
}
