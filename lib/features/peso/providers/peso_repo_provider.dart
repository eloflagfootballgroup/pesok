import 'package:pesok/features/peso/repo/peso_repo.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'peso_repo_provider.g.dart';

@riverpod
PesoRepo pesoRepo(PesoRepoRef ref) {
  return PesoRepo();
}
