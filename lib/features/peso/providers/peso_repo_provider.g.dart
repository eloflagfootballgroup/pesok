// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'peso_repo_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$pesoRepoHash() => r'd4db527eac24d1510a38823e893f3e018244fc1d';

/// See also [pesoRepo].
@ProviderFor(pesoRepo)
final pesoRepoProvider = AutoDisposeProvider<PesoRepo>.internal(
  pesoRepo,
  name: r'pesoRepoProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$pesoRepoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef PesoRepoRef = AutoDisposeProviderRef<PesoRepo>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
