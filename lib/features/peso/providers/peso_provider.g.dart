// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'peso_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$pesoHash() => r'049758b8c0312e89d1913e500434bd14d668b45d';

/// See also [Peso].
@ProviderFor(Peso)
final pesoProvider =
    AutoDisposeNotifierProvider<Peso, List<PesoModelHive>?>.internal(
  Peso.new,
  name: r'pesoProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$pesoHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Peso = AutoDisposeNotifier<List<PesoModelHive>?>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
