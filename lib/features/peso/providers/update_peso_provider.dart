import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pesok/features/peso/models/peso_model.dart';
import 'package:pesok/features/peso/models/peso_model_hive.dart';

final pesoUpdateFlagProvider = StateProvider<PesoModelHive>(
  (ref) =>
      PesoModelHive(fechaDeCreacion: DateTime.now(), peso: 0.0, pesoMedio: 0.0),
);
