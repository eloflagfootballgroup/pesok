import 'package:hive_flutter/hive_flutter.dart';
import 'package:pesok/features/peso/models/peso_model_hive.dart';
import 'dart:developer' as developer;

class PesoRepo {
  late Box<PesoModelHive> _hive;
  late List<PesoModelHive> _box;
  PesoRepo();
  List<PesoModelHive> getValue() {
    _hive = Hive.box<PesoModelHive>('pesos_box');
    _box = _hive.values.toList();
    return _box;
  }

  List<PesoModelHive> addValue(PesoModelHive peso) {
    _hive.add(peso);
    return _hive.values.toList();
  }

  List<PesoModelHive> removeValue(String id) {
    //borrar, solo para log
    PesoModelHive? _pesoABorrar;
    developer
        .log('Pesoaborrar longitud de Hive: ${_hive.values.toList().length}');
    for (PesoModelHive peso in _hive.values.toList()) {
      developer.log("for de pesos a borrar: ++${peso.id}");
      if (peso.id == id) _pesoABorrar = peso;
    }
    developer
        .log("--removeValue id: $id\n--removeValue elemento: $_pesoABorrar");
    developer.log(
        "--removeValue indexWhere: ${_hive.values.toList().indexWhere((element) => element.id == id)}");

    _hive.deleteAt(
        _hive.values.toList().indexWhere((element) => element.id == id));
    return _hive.values.toList();
  }

  List<PesoModelHive> updateValue(int index, PesoModelHive peso) {
    _hive.putAt(index, peso);
    return _hive.values.toList();
  }

  void deleteAll() {
    _box.clear();
  }
}
