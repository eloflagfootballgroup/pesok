import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

// part 'peso_model.freezed.dart';
// part 'peso_model.g.dart';

// @freezed
// class PesoModelHive extends HiveObject with _$PesoModel {
//   PesoModelHive._();
//   @HiveType(typeId: 5, adapterName: 'PesoModelAdapter')
//   factory PesoModelHive({
//     @HiveField(1) @Default('defaultValue') String id,
//     @HiveField(2) DateTime? fechaDeCreacion,
//     @HiveField(3) @Default(0.0) double peso,
//     @HiveField(4) @Default(0.0) double pesoMedio,
//   }) = _PesoModel;

//   factory PesoModelHive.fromJson(Map<String, Object?> json) =>
//       _$PesoModelFromJson(json);
// }
