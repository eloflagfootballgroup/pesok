// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'peso_model_hive.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PesoModelHiveAdapter extends TypeAdapter<PesoModelHive> {
  @override
  final int typeId = 1;

  @override
  PesoModelHive read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PesoModelHive(
      fechaDeCreacion: fields[2] as DateTime?,
      peso: fields[3] as double?,
      pesoMedio: fields[4] as double?,
    )..id = fields[1] as String;
  }

  @override
  void write(BinaryWriter writer, PesoModelHive obj) {
    writer
      ..writeByte(4)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.fechaDeCreacion)
      ..writeByte(3)
      ..write(obj.peso)
      ..writeByte(4)
      ..write(obj.pesoMedio);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PesoModelHiveAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
