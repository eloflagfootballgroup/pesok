import 'package:uuid/uuid.dart';
import 'package:hive/hive.dart';

part 'peso_model_hive.g.dart';

@HiveType(typeId: 1)
class PesoModelHive extends HiveObject {
  @HiveField(1)
  String id;
  @HiveField(2)
  DateTime? fechaDeCreacion;
  @HiveField(3)
  double? peso;
  @HiveField(4)
  double? pesoMedio;
  PesoModelHive({this.fechaDeCreacion, this.peso, this.pesoMedio})
      : id = const Uuid().v4();

  PesoModelHive copyWith({
    String? id,
    DateTime? fechaDeCreacion,
    double? peso,
    double? pesoMedio,
  }) {
    return PesoModelHive(
      fechaDeCreacion: fechaDeCreacion ?? this.fechaDeCreacion,
      peso: peso ?? this.peso,
      pesoMedio: pesoMedio ?? this.pesoMedio,
    );
  }
}
