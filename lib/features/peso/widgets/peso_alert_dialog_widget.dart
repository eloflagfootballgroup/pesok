import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pesok/features/peso/models/peso_model.dart';
import 'package:pesok/features/peso/models/peso_model_hive.dart';
import 'package:pesok/features/peso/providers/peso_provider.dart';
import 'package:pesok/features/peso/providers/update_peso_provider.dart';

import 'dart:developer' as developer;

class PesoAlertDialog extends ConsumerWidget {
  PesoAlertDialog({super.key, required this.fromUpdate});
  final fromUpdate;

  TextEditingController dateController = TextEditingController();
  var texto = 'antes';
  DateTime? fechaFinal;
  double? cifraPesoFinal;
  String? id;
  DateTime initDate = DateTime.now();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // Creamos una key para el formulario
    final _formKey = GlobalKey<FormState>();
    // Variable para almacenar el valor del campo de peso
    String peso = '';

    PesoModelHive pesoUpdate = ref.watch(pesoUpdateFlagProvider);
    id = pesoUpdate.id;
    if (fromUpdate) {
      initDate = pesoUpdate.fechaDeCreacion!;
      dateController.text = initDate.toString().split(" ").first;
      fechaFinal = initDate;
    }

    return AlertDialog(
      title: Text(texto),
      // title: Text(widget.fromUpdate ? 'Actualizar' : 'Nuevo peso'),
      content: Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            // Peso
            TextFormField(
              initialValue: fromUpdate
                  ? pesoUpdate.peso.toString().length > 5
                      ? pesoUpdate.peso.toString().substring(0, 5)
                      : pesoUpdate.peso.toString()
                  : "",
              decoration: const InputDecoration(labelText: 'Peso'),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Por favor ingresa tu peso';
                }
                peso = value;
                cifraPesoFinal = double.parse(value);
                return null;
              },
            ),
            Visibility(
              visible: fromUpdate,
              // Fecha
              child: TextFormField(
                controller: dateController,
                decoration: const InputDecoration(
                  labelText: 'Fecha',
                  prefixIcon: Icon(Icons.calendar_today_rounded),
                ),
                onTap: () async {
                  DateTime? pickedDate = await showDatePicker(
                    context: context,
                    initialDate: initDate,
                    firstDate: DateTime(2023),
                    lastDate: DateTime(2100),
                  );
                  String fechaAMostrar = '';
                  if (pickedDate == null) {
                    fechaAMostrar = initDate.toString().split(" ").first;
                    fechaFinal = initDate;
                    developer
                        .log("if (pickedDate == null) fechaFinal: $fechaFinal");
                  } else {
                    fechaAMostrar = pickedDate.toString().split(" ").first;
                    fechaFinal = pickedDate;
                    developer.log("else fechaFinal: $fechaFinal");
                  }
                  dateController.text = fechaAMostrar;
                },
              ),
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: const Text('Cancelar'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        TextButton(
          child: const Text('Guardar'),
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              // Aquí puedes manejar la lógica para guardar los datos
              fromUpdate
                  ? ref.read(pesoProvider.notifier).updatePeso(
                      1,
                      PesoModelHive(
                          fechaDeCreacion: fechaFinal!,
                          peso: cifraPesoFinal!,
                          pesoMedio: 0.0))
                  : ref.read(pesoProvider.notifier).addPeso(peso);
              Navigator.of(context).pop();
            }
          },
        ),
      ],
    );
  }
}
