import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pesok/features/peso/models/peso_model_hive.dart';
import 'package:pesok/features/peso/providers/peso_provider.dart';
import 'package:pesok/features/peso/providers/update_peso_provider.dart';
import 'dart:developer' as developer;

import 'package:pesok/features/peso/widgets/peso_alert_dialog_widget.dart';

class PesoScreen extends ConsumerWidget {
  const PesoScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    List<PesoModelHive>? listaPesos = ref.watch(pesoProvider);
    List<DataRow> rowsList = [];

    void fillCells(listaPesosParam) {
      for (PesoModelHive peso in listaPesosParam) {
        developer.log("= Screen Peso.id ${peso.id}");
        rowsList.add(DataRow(
          onLongPress: () {
            ref.read(pesoUpdateFlagProvider.notifier).state = peso;

            showDialog(
              context: context,
              builder: (BuildContext context) {
                // Creamos una key para el formulario
                final _formKey = GlobalKey<FormState>();
                // Variable para almacenar el valor del campo de peso
                String peso = '';
                return PesoAlertDialog(
                  fromUpdate: true,
                );
              },
            );
          },
          cells: [
            DataCell(Text(peso.fechaDeCreacion.toString().substring(0, 10))),
            // DataCell(Text(peso.peso.toString().substring(0, 6))),
            DataCell(peso.peso.toString().length > 5
                ? Text(peso.peso.toString().substring(0, 5))
                : Text(peso.peso.toString())),
            DataCell(peso.pesoMedio != 0.0
                ? peso.pesoMedio.toString().length > 5
                    ? Text(peso.pesoMedio.toString().substring(0, 5))
                    : Text(peso.pesoMedio.toString())
                : const Text('-')),
            DataCell(IconButton(
              icon: const Icon(Icons.delete_outlined),
              onPressed: () {
                developer.log(
                    'Boton delete\nPeso Id: ${peso.id}\tPeso peso: ${peso.peso}');
                ref.read(pesoProvider.notifier).deletePeso(peso.id);
              },
            )),
          ],
        ));
      }
    }

    fillCells(listaPesos);

    return Scaffold(
      appBar: AppBar(
        title: const Text('PesoK'),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Container(
          width: double.infinity,
          child: DataTable(columns: const [
            DataColumn(label: Text('Fecha')),
            DataColumn(label: Text('Peso')),
            DataColumn(label: Text('Media')),
            DataColumn(label: Text(''))
          ], rows: rowsList),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // Creamos una key para el formulario
              final _formKey = GlobalKey<FormState>();
              // Variable para almacenar el valor del campo de peso
              String peso = '';
              return PesoAlertDialog(
                fromUpdate: false,
              );
            },
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
