# pesok


App para registrar el peso diario y calcular la media de los últimos 5 días
## Descripción:

Esta aplicación Flutter te permite registrar tu peso diario y calcular la media de los últimos 5 días. Es una herramienta simple y útil para aquellos que quieren controlar su peso y llevar un seguimiento de su progreso.

## Funcionalidades:

### MVP:
- Registrar tu peso diario
- Ver un historial de tus registros
- Calcular la media de tu peso de los últimos 5 días
### Backlog:
- Visualizar tu progreso en un gráfico
- Exportar tus datos a un archivo CSV